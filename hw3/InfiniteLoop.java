/*
    This testcase attempts to verify correctness of context sensitivity and widening functionality.
    It also cannot be analyzed by simply running the program and checking the output, as the loop is
    actually infinite.
*/
class Main {
    public static void main(String[] args) {
        int n1;
        n1 = 0 - 1;
        System.out.println((new M()).get(1, 1));
        System.out.println(n1 * (new N().get(n1, n1)));
    }
}

class M {
    public boolean doContinue() {
        return true;
    }
    public int get(int o, int d) {
        while (this.doContinue())
            o = o + d;
        return o;
    }
}
class N extends M {
    public boolean doContinue() {
        return false;
    }
}